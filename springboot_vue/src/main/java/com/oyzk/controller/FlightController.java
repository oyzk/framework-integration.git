package com.oyzk.controller;

import com.oyzk.entity.FlightEntity;
import com.oyzk.service.FlightService;
import com.oyzk.utils.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Kingkang
 * @title FlightController
 * @create 2022/12/13
 **/
@Controller
public class FlightController {

    @RequestMapping("/test")
    @ResponseBody
    public String test(){
        return "test111";
    }

    @GetMapping("/")
    public String indexPage() {
        return "index"; //返回一个index.html页面
    }

    @Autowired
    private FlightService flightService;

    /**
     * 查询所有信息
     * @param
     * @return
     */
    @PostMapping("/getAllFlight")
    @ResponseBody
    public ResultData getAllFlight(){
        List<FlightEntity> all = flightService.findAll();
        return ResultData.success(all);
    }


    /**
     * 新增
     * @param flightEntity
     * @return
     */
    @RequestMapping(value = "/saveFlight", method = RequestMethod.POST)
    @ResponseBody
    public ResultData saveFlight(@RequestBody FlightEntity flightEntity){
        if (flightEntity != null) {
            flightService.insertFlight(flightEntity);
            return ResultData.fail(200,"操作成功");
        }
        return ResultData.fail(500,"参数为空");
    }



    /**
     * 更新
     * @param flightEntity
     * @param
     * @return
     */
    @RequestMapping(value = "/toUpdateFlight", method = RequestMethod.POST)
    @ResponseBody
    public ResultData toUpdateFlight(@RequestBody FlightEntity flightEntity){
        if (flightEntity != null) {
            flightService.updateFligth(flightEntity);
            return ResultData.fail(200,"操作成功");
        }
        return ResultData.fail(500,"参数为空");
    }

    /**
     * 删除
     * @param
     * @return
     */
  //  @RequestMapping(value = "/deleteFlight", method = RequestMethod.GET)
    @GetMapping("/deleteFlight")
    @ResponseBody
    public ResultData deleteFlight(Integer id){
        if(id.equals("")){
            return ResultData.fail(500,"参数为空");
        }
        flightService.deleteByIdFligth(id);
        return ResultData.fail(200,"操作成功");
    }

}
