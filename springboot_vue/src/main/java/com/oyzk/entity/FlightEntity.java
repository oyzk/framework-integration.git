package com.oyzk.entity;

import lombok.Data;

/**
 * @author Kingkang
 * @title FlightEntity
 * @create 2022/12/13
 **/
@Data
public class FlightEntity {

    private int id;
    private String flightId;
    private String company;
    private String departureAirport;
    private String arriveAirport;

    private String departureTime;
    private String arriveTime;
    private String model;
    private int isDelete;
}
