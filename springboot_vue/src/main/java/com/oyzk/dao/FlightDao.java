package com.oyzk.dao;

import com.oyzk.entity.FlightEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Kingkang
 * @title FlightService
 * @create 2022/12/13
 **/
@Repository
public interface FlightDao {
    /**
     * 查询所有信息
     * MyBatis为我们提供了注解@Results,@Result,@ResultMap
     * 用来代替mapper文件中的<resultMap>,<result>,<select id="showAll" resultMap="id">
     * @return
     */
    @Select("select*from gd_flight")
    @Results(id="floghtResultMap", value={
            @Result(column = "id",property = "id"),
            @Result(column = "flight_id",property = "flightId"),
            @Result(column = "company",property = "company"),
            @Result(column = "departure_airport",property = "departureAirport"),
            @Result(column = "arrive_airport",property = "arriveAirport"),
            @Result(column = "departure_time",property = "departureTime"),
            @Result(column = "arrive_time",property = "arriveTime"),
            @Result(column = "model",property = "model"),
            @Result(column = "is_delete",property = "isDelete")
    }
    )
    List<FlightEntity> findAll();


    @Insert("INSERT INTO gd_flight(`flight_id`, `company`, `departure_airport`, `arrive_airport`, `departure_time`, `arrive_time`, `model`, `is_delete`) " +
            "VALUES(#{flightId},#{company},#{departureAirport},#{arriveAirport},#{departureTime},#{arriveTime},#{model},#{isDelete});")
    int insertFlight(FlightEntity flightEntity);

    @Update("UPDATE gd_flight set " +
            " company=#{company},flight_id=#{flightId}, " +
            " departure_airport=#{departureAirport},arrive_airport=#{arriveAirport}," +
            " departure_time=#{departureTime},arrive_time=#{arriveTime}," +
            " model=#{model},is_delete=#{isDelete}"+
            " where id=#{id}")
    int updateFligth(FlightEntity flightEntity);

    @Delete("delete from gd_flight where id=#{id}")
    int deleteByIdFligth(int id);
}
