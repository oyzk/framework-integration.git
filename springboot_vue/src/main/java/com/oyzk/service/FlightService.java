package com.oyzk.service;

import com.oyzk.entity.FlightEntity;

import java.util.List;

/**
 * @author Kingkang
 * @title FlightService
 * @create 2022/12/13
 **/
public interface FlightService {
    List<FlightEntity> findAll();



    int insertFlight(FlightEntity flightEntity);

    int updateFligth(FlightEntity flightEntity);

    int deleteByIdFligth(int id);
}
