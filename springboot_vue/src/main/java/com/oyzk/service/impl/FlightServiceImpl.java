package com.oyzk.service.impl;

import com.oyzk.dao.FlightDao;
import com.oyzk.entity.FlightEntity;
import com.oyzk.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Kingkang
 * @title FlightServiceImpl
 * @create 2022/12/13
 **/
@Service
public class FlightServiceImpl implements FlightService {

    @Autowired
    private FlightDao flightDao;
    @Override
    public List<FlightEntity> findAll() {
        return flightDao.findAll();
    }

    @Override
    public int insertFlight(FlightEntity flightEntity) {
        return flightDao.insertFlight(flightEntity);
    }
    @Override
    public int updateFligth(FlightEntity flightEntity) {
        return flightDao.updateFligth(flightEntity);
    }
    @Override
    public int deleteByIdFligth(int id) {
        return flightDao.deleteByIdFligth(id);
    }
}
