package com.oyzk;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Kingkang
 * @title FlightApplication
 * @create 2022/12/13
 **/
@SpringBootApplication
@MapperScan("com.oyzk.dao")
public class FlightApplication {
    public static void main(String[] args) {
        SpringApplication.run(FlightApplication.class,args);
    }
}
