package com.oyzk.utils;

import lombok.Data;

@Data
public class ResultData<T> {
    /** 状态码 */
    private int status ;
    /** 返回的操作信息 */
    private String message ;
    /** 返回的数据信息 */
    private T data ;
    /** 记录当前操作时间 */
    private long timestamp ;

    public ResultData() {
        this.timestamp = System.currentTimeMillis();
    }

    /**统一结果返回 成功时 */
    public static <T> ResultData<T> success(T data){
        ResultData<T> resultData = new ResultData<>();
        resultData.setStatus(200);
        resultData.setMessage("操作成功");
        resultData.setData(data);
        return resultData;
    }

    /**统一结果返回 失败时 */
    public static <T> ResultData<T> fail(int code,String message){
        ResultData<T> resultData = new ResultData<>();
        resultData.setStatus(code);
        resultData.setMessage(message);
        return resultData;
    }
}
