package com.oyzk.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Kingkang
 * @title Person
 * @create 2022/11/28
 **/
@Setter
@Getter
@Entity
//和数据库表对应
@Table(name="Person")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="username")
    private String username;

    @Column(name="address")
    private String address;

    @Column(name = "phone")
    private String phone;


    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
