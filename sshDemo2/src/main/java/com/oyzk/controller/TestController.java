package com.oyzk.controller;

import com.oyzk.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Kingkang
 * @title TestController
 * @create 2022/11/28
 **/
@Controller
public class TestController {

    //自动注入
    @Autowired
    private TestService testService;
    @RequestMapping("/testSpringMvc")
    public String testSpringMvc(){
        //实际返回的是views/test.jsp ,spring-mvc.xml中配置过前后缀
        return "test";
    }

    @RequestMapping("/testSpring")
    @ResponseBody
    public String testSpring(){
        String test = testService.test();
        return test;
    }
}
