package com.oyzk.service;

/**
 * @author Kingkang
 * @title TestService
 * @create 2022/11/28
 **/
public interface TestService {
    String test();
}
