<%--
  Created by IntelliJ IDEA.
  User: Kingkang
  Date: 2022/12/7
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>信息展示页面</title>
    <link type="text/css" rel="stylesheet" href="../../css/bootstrap.css">
    <script type="text/javascript" src="../../js/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-headr">
                <h1>
                    <small>信息列表 ———————— 显示所有人员信息</small>
                </h1>
                <span style="color:#FAF520 ;font-weight: bold">${flageMsg}</span>
            </div>
        </div>
        <div class="col-md-4 column">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/flight/saveFlightPage">新增</a>
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/flight/getAllFlight">显示全部信息</a>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form class="form-inline" action="${pageContext.request.contextPath}/flight/getFlightById" method="get" style="float:right">
                <span style="color:#FAF520 ;font-weight: bold">${error}</span>
                <input type="text" name="id" class="form-control" placeholder="请输入要查询的人员id">
                <input type="submit" value="查询" class="btn btn-primary">
            </form>
        </div>

    </div>

    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>航班号</th>
                    <th>航班公司</th>
                    <th>出发地</th>
                    <th>目的地</th>
                    <th>出发时间</th>
                    <th>到达时间</th>
                    <th>飞机类型</th>
                    <th>是否删除</th>
                    <th>操作</th>
                </tr>
                </thead>

                <%--                        人员从数据库中查询出来--%>
                <tbody>
                <c:forEach items="${infoList}" var="flight">
                    <tr>
                        <td>${flight.id}</td>
                        <td>${flight.flightId}</td>
                        <td>${flight.company}</td>
                        <td>${flight.departureAirport}</td>
                        <td>${flight.arriveAirport}</td>
                        <td>${flight.departureTime}</td>
                        <td>${flight.arriveTime}</td>
                        <td>${flight.model}</td>
                        <td>${flight.isDelete}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/flight/toUpdateFlightPage?id=${flight.id}">修改</a>
                            &nbsp; ||&nbsp;
                            <a href="${pageContext.request.contextPath}/flight/deleteFlight?id=${flight.id}">刪除</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>

</html>

