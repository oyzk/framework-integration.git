<%--
  Created by IntelliJ IDEA.
  User: Kingkang
  Date: 2022/12/7
  Time: 20:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>修改人员信息页面</title>
    <link type="text/css" rel="stylesheet" href="../../css/bootstrap.css">
    <script type="text/javascript" src="../../js/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-headr">
                <h1>
                    <samll>修改人员</samll>
                </h1>
            </div>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/flight/toUpdateFlight" method="post">

        <input type="hidden" name="id" value="${flight.id}">
        <div class="form-group">
            <table>id</table>
            <input type="text" name="id" class="form-control" value="${flight.id}" disabled>
        </div>
        <div class="form-group">
            <table>航班号</table>
            <input type="text" name="flightId" class="form-control" value="${flight.flightId}"required>
        </div>
        <div class="form-group">
            <table>航班公司</table>
            <input type="text" name="company" class="form-control"  value="${flight.company}" required>
        </div>
        <div class="form-group">
            <table>出发地</table>
            <input type="text" name="departureAirport" class="form-control" value="${flight.departureAirport}" required>
        </div>
        <div class="form-group">
            <table>目的地</table>
            <input type="text" name="arriveAirport" class="form-control" value="${flight.arriveAirport}" required>
        </div>
        <div class="form-group">
            <table>出发时间</table>
            <input type="date" name="departureTime" class="form-control" value="${flight.departureTime}" required>
        </div>
        <div class="form-group">
            <table>到达时间</table>
            <input type="date" name="arriveTime" class="form-control" value="${flight.arriveTime}" required>
        </div>
        <div class="form-group">
            <table>飞机类型</table>
            <input type="text" name="model" class="form-control" value="${flight.model}" required>
        </div>
        <div class="form-group">
            <table>是否删除</table>
            <input type="text" name="isDelete" class="form-control" value="${flight.isDelete}" required>
        </div>
        <div class="form-group">
            <input type="submit" class="form-control" value="修改">
        </div>
    </form>

</div>
</body>
</html>

