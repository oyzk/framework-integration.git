package com.oyzk.test;

import com.oyzk.dao.FlightDao;
import com.oyzk.entity.FlightEntity;
import com.oyzk.service.FlightService;
import com.oyzk.service.impl.FlightServiceImpl;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author Kingkang
 * @title FlightTest
 * @create 2022/12/7
 **/
public class FlightTest {
    @Test
    public void  findAllTest(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        FlightService service = (FlightService) applicationContext.getBean("FlightService"); // 因为给service起了别名，所以通过id的方式获取class
        service.findAll();

    }

    @Test
    public void test() throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(resourceAsStream);
        SqlSession session = factory.openSession(true);
        FlightDao mapper = session.getMapper(FlightDao.class);
        List<FlightEntity> all = mapper.findAll();
        for (FlightEntity flight : all) {
            System.out.println(flight);
        }
    }

}
