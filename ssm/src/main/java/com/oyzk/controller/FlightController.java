package com.oyzk.controller;

import com.oyzk.entity.FlightEntity;
import com.oyzk.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kingkang
 * @title FlightController
 * @create 2022/12/7
 **/
@Controller
@RequestMapping("/flight")
public class FlightController {

    @RequestMapping("/findAll")
    public String findAll(){
        System.out.println("success");
        return "homePage";
    }

    @Autowired
    private FlightService flightService;

    /**
     * 查询所有信息
     * @param module
     * @return
     */
    @RequestMapping(value = "/getAllFlight", method = RequestMethod.GET)
    public  String getAllFlight(Model module){
        List<FlightEntity> all = flightService.findAll();
        module.addAttribute("infoList",all);
        return "homePage";
    }

    /**
     *@description TODO
     *通过id查询
     *@return org.springframework.web.servlet.ModelAndView
     *@author Kingkang
     *@time 2022/11/27
     */
    @RequestMapping(value = "/getFlightById", method = RequestMethod.GET)
    public ModelAndView getFlightById(int id){
        ModelAndView modelAndView=new ModelAndView();
        FlightEntity flightById = flightService.findById(id);
        List<FlightEntity> flightEntities=new ArrayList<FlightEntity>();
        flightEntities.add(flightById);
        modelAndView.addObject("infoList",flightEntities);
        //跳转信息展示页面
        modelAndView.setViewName("homePage");
        return modelAndView;
    }

    /**
     * 跳转新增页面
     * @return
     */
    @RequestMapping(value = "/saveFlightPage", method = RequestMethod.GET)
    public String saveFlightPage(){
        return "addFlight";
    }
    /**
     * 新增
     * @param flightEntity
     * @return
     */
    @RequestMapping(value = "/saveFlight", method = RequestMethod.POST)
    public String saveFlight(FlightEntity flightEntity){
        flightService.insertFlight(flightEntity);
        return "redirect:/flight/getAllFlight";
    }

    /**
     * 通过id查询数据，在跳转到更新页面
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/toUpdateFlightPage", method = RequestMethod.GET)
    public String toUpdateFlightPage(int id,Model model){
        FlightEntity flight = flightService.findById(id);
        model.addAttribute("flight",flight);
        return "updateFlight";
    }

    /**
     * 更新
     * @param flightEntity
     * @param model
     * @return
     */
    @RequestMapping(value = "/toUpdateFlight", method = RequestMethod.POST)
    public String toUpdateFlight(FlightEntity flightEntity,Model model){
        int i = flightService.updateFligth(flightEntity);
        if (i>0){
            model.addAttribute("flageMsg","更新成功");
        }else{
            model.addAttribute("flageMsg","更新失败");
        }
        return "redirect:/flight/getAllFlight";
    }

    /**
     * 删除
     * @param
     * @return
     */
    @RequestMapping(value = "/deleteFlight", method = RequestMethod.GET)
    public String deleteFlight(int id){
        flightService.deleteByIdFligth(id);
        return "redirect:/flight/getAllFlight";
    }



}
