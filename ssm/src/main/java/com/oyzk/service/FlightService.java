package com.oyzk.service;

import com.oyzk.entity.FlightEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author Kingkang
 * @title FlightService
 * @create 2022/12/7
 **/
public interface FlightService {
      List<FlightEntity> findAll();

      FlightEntity findById(int id);

      int insertFlight(FlightEntity flightEntity);

      int updateFligth(FlightEntity flightEntity);

      int deleteByIdFligth(int id);
}
