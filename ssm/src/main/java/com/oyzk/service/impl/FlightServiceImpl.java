package com.oyzk.service.impl;

import com.oyzk.dao.FlightDao;
import com.oyzk.entity.FlightEntity;
import com.oyzk.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Kingkang
 * @title FlightServiceImpl
 * @create 2022/12/7
 **/
//@Service("FlightService")//取个别名，测试需要，
@Service
public class FlightServiceImpl implements FlightService {

    @Autowired
    private FlightDao flightDao;

    public List<FlightEntity> findAll() {
        return flightDao.findAll();
    }

    public FlightEntity findById(int id) {
        return flightDao.findById(id);
    }

    public int insertFlight(FlightEntity flightEntity) {
        return flightDao.insertFlight(flightEntity);
    }

    public int updateFligth(FlightEntity flightEntity) {
        return flightDao.updateFligth(flightEntity);
    }

    public int deleteByIdFligth(int id) {
        return flightDao.deleteByIdFligth(id);
    }
  /*  public List<FlightEntity> findAll() {
        System.out.println("查询所有用户。。。");
        return null;
    }*/
}
