package com.oyzk.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author Kingkang
 * @title FlightEntity
 * @create 2022/12/6
 **/
@Data
public class FlightEntity {

    private int id;
    private String flightId;
    private String company;
    private String departureAirport;
    private String arriveAirport;
    /**
     * 实例
     * 把从前端到后端的日期格式化
     * timezone="GMT+8" 东八区
     * pattern:是你需要转换的时间日期的格式
     * 		 "yyyy-MM-dd HH:mm:ss"
     * 		 "yyyy-MM-dd HH:mm"
     * 		 "yyyy-MM-dd HH"
     * 		 "yyyy-MM-dd"
     * 		 "yyyy-MM"
     * 根据需要自行选择
     */
/*    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
    private Date departureTime;
    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
    private Date arriveTime;*/

    private String departureTime;
    private String arriveTime;
    private String model;
    private int isDelete;
}
